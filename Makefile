TARGET=rally2p
export PATH:=tools:$(PATH)

all:
	make -C tools
	make $(TARGET).dsk

ralbin.bin:
	iDSK original/rally2.dsk -g ralbin.bin

patch.bin: patch.asm
	pasmo --bin $^ $@

$(TARGET).dsk: assets/rally2.bas patch.bin ralbin.bin
	iDSK $@ -n
	iDSK rally2p.dsk -i assets/rally2.bas -t 1
	iDSK rally2p.dsk -i patch.bin -t 1
	iDSK rally2p.dsk -i ralbin.bin -t 1

$(TARGET).cdt: assets/rally2.bas patch.bin ralbin.bin 
	2cdt -n -F 0 assets/rally2.bas -r RALLY2.BAS $@
	2cdt patch.bin -r PATCH.BIN $@
	2cdt ralbin.bin -r RALBIN.BIN $@