# Amstrad CPC Grand Prix Rally II keyboard patch

This repository contains the code of a patch that enables playing the CPC game [Grand Prix Rally II](https://www.cpcwiki.eu/index.php/Grand_Prix_Rally_II) using the keyboard (OPQA<SPACE>).

As the instructions of the game shows, the original game could be played only using the JOYSTICK.

The patch consists in replacing the OS KM_GET_JOYSTICK routine by our own that reads the keyboard keys (OPQA and SPACE) instead of the joystick.

The original game uses the firmware to read the joystick, e.g. KM_GET_JOYSTICK. A disassembly of the KM_GET_JOYSTICK shows that the routine is evaluating the keyboard buffer to determine the 2 joystick ports state. The keyboard buffer location depends on the OS version and machine.

The keyboard buffer is updated by the OS interrupt. The keyboard buffer address of the JOY2 is &b4f1 and th one for JOY1 is &b4f4. That means that the keyboard buffer is starting at &b4f4 - 9.
See [Keyboard_scanning](https://www.cpcwiki.eu/index.php/Programming:Keyboard_scanning)

```
{
; &BB24 - KM GET JOYSTICK
;
; CPC464
lBB24   RST $08
        DW $9c5c

    org #1c5c
    ld a,(#b4f1)   ; JOY2 port state
    and #7f
    ld l,a
    ld a,(#b4f4)   ; JOY1 port state
    and #7f
    ld h,a
    ret
}
```

The patch simply replaces the original KM_GET_JOYSTICK vector by our own routine that evaluates the keyboard buffer and returns the state of the keys (OPQA and SPACE) state intead of JOY1_LEFT,JOY1_RIGHT, JOY1_UP,JOY1_DOWN and JOY1_FIRE

The code of the patch is located just below the code of the original game which starts at 1000.


## Requirements

 - GNU Make (others may work)
 - a POSIX compatible environment

## Building

To build the DSK image, run:

```    
    make
```

After a successful build, the dsk file rally2p.dsk can be used in an emulator and you can play using the keys (OPQA and SPACE)

![Now you can press SPACE to start the game](assets/rally2.png)

## TODO

The cdt generation works but unfortunately the BASIC code is throwing a "Memory full"...
