PATCH_RELOC_ADDR equ 900
KM_GET_JOYSTICK	 equ $BB24
kl_l_rom_enable	 equ $b906
kl_rom_restore   equ $b90c

; The patch consists in replacing the OS KM_GET_JOYSTICK routine 
; by our own that reads the keyboard (OPQA<SPACE>) instead of the 
; joystick.

; The game uses the firmware to read the joystick, e.g. KM_GET_JOYSTICK
; A disassembly of the KM_GET_JOYSTICK shows that the routine is
; evaluating the keyboard buffer to determine the 2 joystick ports state
; The keyboard buffer is updated by the OS interrupt
; The keyboard buffer address of the JOY2 is &b4f1 and th one for 
; JOY1 is &b4f4.
; That means that the keyboard buffer is starting at &b4f4 - 9.
; See https://www.cpcwiki.eu/index.php/Programming:Keyboard_scanning)
; The patch simply replaces the original KM_GET_JOYSTICK vector
; by our own routine that evaluates the keyboard buffer and returns
; the state of the (OPQA<SPACE>) states intead of JOY1_LEFT,JOY1_RIGHT,
; JOY1_UP,JOY1_DOWN and JOY1_FIRE
; The code of the patch is located just below the code of the original
; game which starts at 1000.
;
; &BB24 - KM GET JOYSTICK
;
; CPC464
; lBB24   RST $08
;         DW $9c5c
;
;    org #1c5c
;    ld a,(#b4f1)   ; JOY2 port state
;    and #7f
;    ld l,a
;    ld a,(#b4f4)   ; JOY1 port state
;    and #7f
;    ld h,a
;    ret


    org $8000

rom_previous_state:
    ld hl,patch
    ld de,PATCH_RELOC_ADDR
    ld bc,patch_end-patch
    ldir

    call kl_l_rom_enable
    ld (rom_previous_state),a
    ld hl,(KM_GET_JOYSTICK+1)   ; HL points to KM_GET_JOYSTICK routine
    ld a,h
    and $7f
    ld h,a
    inc hl                      ; HL point to key buffer JOY2 state
    ld e,(hl)
    inc hl
    ld d,(hl)
    ex de,hl
    ld (PATCH_RELOC_ADDR+4),hl  ; store the key buffer JOY2 address to use by our routine
    ld a,(rom_previous_state)
    call kl_rom_restore

    ld hl,KM_GET_JOYSTICK       ; Patch the KM_GET_JOYSTICK to invoke our own routine
    ld a,$c3
    ld (KM_GET_JOYSTICK),a 
    ld hl,PATCH_RELOC_ADDR
    ld (KM_GET_JOYSTICK+1),hl

    ret
    
patch
    push ix
patch_1
    ld ix,$0000
    ld a,(ix-3)     ; bit line $43
    and $08         ; state of 'P'
    ld h,a
    ld a,(ix-2)     ; bit line $44
    and $04         ; state of 'O'
    or h
    ld h,a
    ld a,(ix-1)     ; bit line $45
    and $80         ; state of 'space'
    rrca
    rrca
    rrca
    or h
    ld h,a
    ld a,(ix+2)     ; bit line $48
    and $08         ; state of 'Q'
    rrca
    rrca
    rrca
    or h
    ld h,a
    ld a,(ix+2)     ; bit line $48
    and $20         ; state of 'A'
    rrca
    rrca
    rrca
    rrca
    or h
    ld h,a
    pop ix
    ret
patch_end
